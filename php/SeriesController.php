<?php
namespace Drupal\pg_series\Controller;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\PhpStorage\PhpStorageFactory;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\pg_series\Service\SeriesDbService;
se Drupal\pg_series\Service\RolesDbService;

/**
 * Class SeriesController.
 */
class SeriesController extends ControllerBase
{

    protected $dbService;
    protected $rolesDbService;
    public hola()
    {
    echo "chau";
    }
    public function getControllerName()
    {
        return 'pg_series.series_controller';
    }
    
    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        $dbService = $container->get('pg_series.series_db_service');
        $rolesDbService =  $container->get('pg_series.roles_db_service');
        $controller = new static($dbService, $rolesDbService);

        $controller->setStringTranslation($container->get('string_translation'));
        // service( 'language_manager' ) => parent!!!
        return $controller;
    }

    /**
     * Construct new controller.
     *
     * @param \Drupal\pg_series\SeriesDbService $dbService
     *            The repository service.
     * @param \Drupal\pg_series\RolesDbService $rolesDbService
     *            Service that provides access to the roles.
     */
    public function __construct(SeriesDbService $aDbService, RolesDbService $aRolesDbService)
    {
        $this->dbService = $aDbService;
        $this->rolesDbService = $aRolesDbService;
    }

    /**
     * List Series
     *
     * @return array $build
     */
    public function list(Request $request)
    {
        $bid = $request->get('bundle');
        $bundle = $this->dbService->loadBundle($bid, ['name']);
        $entries = $this->dbService->loadSeries4Bundle($bid, ['created', 'id', 'name', 'description', 'period']);

        $build['message'] = array(
            '#markup' => $this->t('series for bundle: %bundle.', ['%bundle' => $bundle->name]),
            '#suffix' => '<br><br>',
        );
        $rows = [];
        $headers = [
            $this->t('created'),
            $this->t('id'),
            $this->t('pgSeTr.att.name'),
            $this->t('pgSeTr.att.description'),
            $this->t('pgSeTr.att.period'),
            $this->t('pgSeTr.att.operations'),
        ];

        foreach ($entries as $entry)
        {
            $series = (array) $entry;

            $options = ['internal' => TRUE];
            $show = Url::fromRoute('pg_series.series_show', ['sid' => $entry->id], $options);
            $list = Url::fromRoute('pg_series.issue_list', ['series' => $entry->id], $options);
            $update = Url::fromRoute('pg_series.series_update', ['sid' => $entry->id], $options);
            $roles = Url::fromRoute('pg_series.series_assign_roles', ['sid' => $entry->id], $options);

            $links = $buttons = [];
            $links['show'] = ['title' => $this->t('pgSeTr.li.showSeries'),
                              'url' =>  $show];
            $links['roles'] = ['title' => $this->t('pgSeTr.li.assignRoles'),
                               'url' =>  $roles];
            $links['list'] = ['title' => $this->t('pgSeTr.li.listIssues'),
                              'url' =>  $list];
            $links['update'] = ['title' => $this->t('pgSeTr.li.updateSeries'),
                                'url' =>  $update];

            $buttons['links'] = ['data' => ['#type' => 'dropbutton',
                                            '#links' => $links],
                                 'width' => '150'];

            $rows[] = array_map(NULL, $series + $buttons);
        }

        $build['table'] = [
            '#type' => 'table',
            '#header' => $headers,
            '#rows' => $rows,
            '#empty' => $this->t('pgSeTr.noEntries'),
        ];

        $build['link'] = array(
            '#prefix' => '<br><div class="nav">',
            '#suffix' => '</div><br>',
        );

        $url = Url::fromRoute('pg_series.series_add', ['bundle' => $bid], ['internal' => TRUE]);
        $build['link']['1'] = array(
            '#title' => $this->t('pgSeTr.li.addSeries'),
            '#type' => 'link',
            '#url' => $url,
            '#class' => "button btn",
            '#prefix' => '<li class="navbar-nav">',
            '#suffix' => '</li>',
        );

        // Don't cache this page.
        $build['#cache']['max-age'] = 0;

        return $build;
    }

    /**
     * @param int $sid
     *        Series ID of series to show
     */
    public function show(int $sid)
    {
        $build = [];
        $series = $this->dbService->loadSeries($sid);

        $build['message'] = array(
            '#markup' => $this->t('Show content of series: %series', ['%series' => $series->name]),
            '#suffix' => '<br>',
        );
        $rows = [];
        $headers = [
            $this->t('key'),
            $this->t('value'),
        ];
        foreach ($series as $key => $value)
        {
            $row = [$key, $value];
            $rows[] = array_map(NULL, $row);
        }
        $build['table'] = [
            '#type' => 'table',
            '#header' => $headers,
            '#rows' => $rows,
            '#empty' => $this->t('pgSeTr.noEntries'),
        ];

        if ($series->name == "Methoden und Begründungen")
        {
            $build['mak'] = $this->showMAK($sid);
        }

        return $build;
    }

    /**
     * Add a new entry to series
     * @return form to render
     */
    public function add(Request $request = NULL)
    {
        $bid = $request->get('bundle');
        // Load the Form
        $build['form'] = $this->formBuilder()->getForm('Drupal\pg_series\Form\series\SeriesAddForm', $bid);

        return $build;
    }

    /**
     * Update entry in series
     * @return form to render
     */
    public function update($sid)
    {
        // Load the Form
        $build['form'] = $this->formBuilder()->getForm('Drupal\pg_series\Form\series\SeriesUpdateForm', $sid);
        
        return $build;
    }

    /**
     * Assign authors to roles in a series
     * @return form to render
     */
    public function assignRoles($sid)
    {
        $user = \Drupal::service('session')->get('user');
        
        // Load the Form
        $build['form'] = $this->formBuilder()->getForm('Drupal\pg_series\Form\series\AssignRolesForm', $sid);
        
        return $build;
    }

    /**
     * @param $sid seriesID used, as PK for the series, where the issue belongs to
     * @param $tab
     * @return toDo
     */
    public function showMAK(int $sid, Request $request = NULL)
    {
        // ToDo: remove when template development is finished
        //drupal_flush_all_caches(); // needed if module is changed
        PhpStorageFactory::get('twig')->deleteAll();// flush Twig cache
        
        $tab = 'about';
        if($request)
        {
            $tab = $request->get('tab');
        }
        // ToDo: use only one request
        if(\Drupal::request()->get('tab'))
        {
            $tab = \Drupal::request()->get('tab');
        }

        $tabs = ['about', 'authors', 'editOffice', 'conflict', 'imprint'];
        foreach ($tabs as $value) {
            $url = Url::fromRoute('pg_series.series_show_tab', ['sid' => 1, 'tab' => $value], ['internal' => TRUE]);
            if($value == $tab)
            {
                $series['tabs'][$value] = $this->loadContent($sid, $value);
                $series['tabs'][$value]['tab_class_active'] = 'active';
            } 
            else
            {
                $series['tabs'][$value]['tab_class_active'] = '';
            }
            $series['tabs'][$value]['tab_link'] = Link::fromTextAndUrl(t('pgSeTr.tab.'. $value), $url);
        }

        $build['template'] = array(
            '#theme' => 'series_show',
            '#title' => t('Content from word-document'),
            '#tabs' => $tabs,
            '#series' => $series,
            '#suffix' => '<br>',
        );

        return $build;
    }

    private function loadContent($sid, $value)
    {
        switch($value)
        {
            case 'about':
                $series = (array)$this->dbService->loadSeries($sid, ['about']);
                $content['tab_name'] = $value;
                $content['tab_template'] = $value;;
                $content['tab_content'] = $series['about'];
                break;
            case 'authors':
                $i = 1;
                $fields = [ 'up_id', 'up_uid', 'up_graduation', 'up_firstname', 'up_lastname',
                    'up_graduation_suffix', 'up_email', 'up_institute', 'up_department', 'up_street',
                    'up_postal_code', 'up_city', 'up_country', 'up_telephone', 'up_orcid', 'up_picture'];
                $result = $this->rolesDbService->loadAuthors4Series($sid, $fields);
                foreach($result as $author)
                {
                    $authArr = (array)$author;
                    $authors[] = (array)$author + ['modal_id' => 'm_authors_'.$i++];
                }
                $content['tab_name'] = $value;
                $content['tab_template'] = $value;
                $content['tab_content'] = $authors;
                break;
            case 'editOffice':
                $i = 1;
                $fields = [ 'up_id', 'up_uid', 'up_graduation', 'up_firstname', 'up_lastname',
                    'up_graduation_suffix', 'up_email', 'up_institute', 'up_department', 'up_street',
                    'up_postal_code', 'up_city', 'up_country', 'up_telephone', 'up_orcid', 'up_picture'];
                $result = $this->rolesDbService->loadEditOffice4Series($sid, $fields);
                foreach($result as $author)
                {
                    $authArr = (array)$author;
                    $authors[] = (array)$author + ['modal_id' => 'm_authors_'.$i++];
                }
                $content['tab_name'] = $value;
                $content['tab_template'] = 'authors';
                $content['tab_content'] = $authors;
                break;
            case 'conflict':
                $series = (array)$this->dbService->loadSeries($sid, ['about']);
                $content['tab_name'] = $value;
                $content['tab_template'] = 'about';
                $content['tab_content'] = "<h3>Conflict of Interest:</h3><p>Befangenheitserklärung</p>";
                break;
            case 'imprint':
                $series = (array)$this->dbService->loadSeries($sid, ['about']);
                $content['tab_name'] = $value;
                $content['tab_template'] = 'about';
                $content['tab_content'] = "<h3>Imprint:</h3><p>DFG</p>";
                break;
            default:
                $content = '';
        }

        return $content;
    }

    /**
     * Testing !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * simple test
     */
    public function test($sid, $tab = null) : array
    {
        $var = 1;
        // return [
        //     '#markup' => $this->t('Check successfull if you can read this: ' .$sid. '.'),
        //     '#suffix' => '<br>',
        // ];
        $header = "This is a test.";
        $body = "BodyCount";
        return [
            'header' => $header,
            'body' => $body,
            '#cache' => ['max-age' => 0]
        ];
    }

    /**
     * Testing !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * check: simple access check if user is logged in.
     * @return string Return Hello string.
     */
    public function check($name)
    {
        return [
            '#markup' => $this->t('Check successfull if you can read this: ' .$name. '.'),
            '#suffix' => '<br>',
        ];
    }
}
