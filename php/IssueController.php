<?php
namespace Drupal\pg_series\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Drupal\pg_series\Service\SeriesDbService;

/**
 * Class IssueController.
 */
class IssueController extends ControllerBase
{
    protected $dbService;

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        $controller = new static($container->get('pg_series.series_db_service'));
        $controller->setStringTranslation($container->get('string_translation'));
        // service( 'language_manager' ) => parent!!!
        return $controller;
    }

    /**
     * Construct a new controller.
     *
     * @param \Drupal\pg_series\SeriesDbService $dbService
     *            The repository service.
     */
    public function __construct(SeriesDbService $aDbService)
    {
        $this->dbService = $aDbService;
    }

    /**
     * Return tht controller name.
     *
     * @return name of controller
     */
    public function getControllerName()
    {
        return 'pg_series.issue_controller';
    }

    /**
     * Index.
     *
     * @return build Return index site for Methods.
     */
    public function index()
    {
        $build['message'] = array(
            '#markup' => $this->t('Add a new issue.'),
            '#suffix' => '<br><br>',
        );
        $build['link'] = array(
            '#prefix' => '<br><div class="nav">',
            '#suffix' => '</div><br>',
        );
        
        $url = Url::fromRoute('pg_series.issue_add');
        $build['link']['1'] = array(
            '#title' => $this->t('pgSeTr.li.addIssue'),
            '#type' => 'link',
            '#url' => $url,
            '#class' => "button btn",
            '#prefix' => '<li class="navbar-nav">',
            '#suffix' => '</li>',
        );

        return $build;
    }

    /**
     * @param $sid seriesID used, as PK for the series, where the issue belongs to 
     * @return toDo
     */
    public function list(Request $request)
    {
        $sid = $request->get('series');
        $series = $this->dbService->loadSeries($sid, ['id', 'name']);
        $issues = $this->dbService->loadIssues4Series($sid, ['created', 'id', 'name', 'number', 'year']);
        
        $content = [];
        $content['message'] = [
            '#markup' => $this->t('issues for series %series.', ['%series' => $series->name]),
        ];
        
        $rows = [];
        $headers = [
            $this->t('created'),
            $this->t('id'),
            $this->t('pgSeTr.att.name'),
            $this->t('volume'),
            $this->t('pgSeTr.att.year'),
            $this->t('pgSeTr.att.operations'),
        ];

        foreach ($issues as $issue)
        {
            $options = ['internal' => TRUE];
            $list = Url::fromRoute('pg_series.article_list', ['issue' => $issue->id], $options);
            $show = Url::fromRoute('pg_series.issue.show', ['iid' => $issue->id], $options);
            $update = Url::fromRoute('pg_series.issue_update', ['iid' => $issue->id], $options);

            $links = $buttons = [];
            $links['list'] = ['title' => $this->t('pgSeTr.li.listArticles'),
                              'url' =>  $list ];
            $links['show'] = ['title' => $this->t('pgSeTr.li.showIssue'),
                              'url' =>  $show];
            $links['update'] = ['title' => $this->t('pgSeTr.li.updateIssue'),
                                'url' =>  $update];
            
            $buttons['links'] = ['data' => ['#type' => 'dropbutton',
                                            '#links' => $links],
                                 'width' => '150'];
            
            $rows[] = array_map(NULL, (array)$issue + $buttons);
        }
        
        $content['table'] = [
            '#type' => 'table',
            '#header' => $headers,
            '#rows' => $rows,
            '#empty' => $this->t('pgSeTr.noEntries'),
        ];
        
        $content['link'] = array(
            '#prefix' => '<br><div class="nav">',
            '#suffix' => '</div><br>',
        );
        
        $url = Url::fromRoute('pg_series.issue_add', ['series' => $series->id], ['internal' => TRUE]);
        $content['link']['1'] = array(
            '#title' => $this->t('pgSeTr.li.addIssue'),
            '#type' => 'link',
            '#url' => $url,
            '#class' => "button btn",
            '#prefix' => '<li class="navbar-nav">',
            '#suffix' => '</li>',
        );
       
        // Don't cache this page.
        $content['#cache']['max-age'] = 0;
        
        return $content;
    }

    /**
     * @param int $iid
     *        Issue ID to show
     */
    public function show(int $iid)
    {
        $build = [];
        $issue = $this->dbService->loadIssue($iid);
        $series = $this->dbService->loadSeries($issue->id, ['id', 'name']);
        
        $build['message'] = array(
            '#markup' => $this->t('Show content of: %name - %issue',
                                  ['%name' => $series->name, '%issue' => $issue->name]),
            '#suffix' => '<br>',
        );
        $rows = [];
        $headers = [
            $this->t('key'),
            $this->t('value'),
        ];
        foreach ($issue as $key => $value)
        {
            $row = [$key, $value];
            $rows[] = array_map(NULL, $row);
        }
        $build['table'] = [
            '#type' => 'table',
            '#header' => $headers,
            '#rows' => $rows,
            '#empty' => $this->t('pgSeTr.noEntries'),
        ];
        
        return $build;
    }
}
